<?php

namespace BonchDev\LaravelUCaller\Tests;

use BonchDev\LaravelUCaller\LaravelUCallerProvider;
use BonchDev\LaravelUCaller\UCallerFacade;
use Orchestra\Testbench\TestCase as TestbenchTestCase;

class TestCase extends TestbenchTestCase
{
    protected function setUp(): void
    {
        parent::setUp();
    }

    protected function getPackageProviders($app)
    {
        return [LaravelUCallerProvider::class];
    }

    protected function getEnvironmentSetUp($app)
    {
        $app['config']->set('ucaller.service_id', ''); // paste service_id
        $app['config']->set('ucaller.key', ''); // paste key
    }

    public function testFacade()
    {
        dump(UCallerFacade::getBalance());
    }
}
