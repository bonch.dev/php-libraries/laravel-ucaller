<?php

namespace BonchDev\LaravelUCaller;

use BonchDev\uCaller\uCaller;
use Illuminate\Support\Facades\Facade;

/**
 * UCallerFacade class
 * 
 * @method uCaller initCall($phone, $code = null, string $client = null, string $unique = null)
 * @method uCaller initRepeat(int $uCallerID)
 * @method uCaller getInfo(int $uCallerID)
 * @method uCaller getBalance()
 */
class UCallerFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'ucaller';
    }
}
