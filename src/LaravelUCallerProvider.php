<?php

namespace BonchDev\LaravelUCaller;

use BonchDev\uCaller\uCaller;
use Illuminate\Support\ServiceProvider;

class LaravelUCallerProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__ . "/../config/ucaller.php" => config_path('ucaller.php')
        ], 'config');
    }

    public function register()
    {
        $this->app->bind('ucaller', function ($app) {
            return new uCaller(
                $app['config']['ucaller.service_id'],
                $app['config']['ucaller.key'],
            );
        });
    }
}
